learn scala, play2 frameowrk for bidding auction web site


1. GIT branches:(http://nvie.com/posts/a-successful-git-branching-model/)
		master - production code
		develop - developer work should be in this branch(feature branch off/back develop branch)
		release-* - release branch is off develop branch, will be deleted after merged back to develop and master

2. create new branch:

$ git checkout -b release-1.2 develop
Switched to a new branch "release-1.2"
$ ./bump-version.sh 1.2
Files modified successfully, version bumped to 1.2.
$ git commit -a -m "Bumped version number to 1.2"
[release-1.2 74d9424] Bumped version number to 1.2
1 files changed, 1 insertions(+), 1 deletions(-)

After creating a new branch and switching to it, we bump the version number. Here, bump-version.sh is a fictional shell script that changes some files in the working copy to reflect the new version. (This can of course be a manual change—the point being that some files change.) Then, the bumped version number is committed.
